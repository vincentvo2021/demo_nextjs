const baseAPI = 'https://hoodwink.medkomtek.net/api'

const post = async (url, data, token) => {
    var myHeaders = new Headers()
    myHeaders.append("Authorization", `Bearer ${token}`)

    var formdata = new FormData()

    for (const name in data) {
        formdata.append(name, data[name]);
    }

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow'
    }

    return new Promise((resolve, reject) => {
        fetch(`${baseAPI}${url}`, requestOptions)
            .then(response => response.text())
            .then(result => resolve(JSON.parse(result)))
            .catch(error => reject(error))
    })
}

const get = async (url, token) => {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${token}`)

    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    return new Promise((resolve, reject) => {
        fetch(`${baseAPI}${url}`, requestOptions)
            .then(response => response.text())
            .then(result => resolve(JSON.parse(result)))
            .catch(error => reject(error))
    })
}

const API = {
    post,
    get
}

export default API