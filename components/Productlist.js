import React, { useContext, useEffect, useState } from "react";

import Swal from "sweetalert2";
import { useRouter } from "next/router";
import withReactContent from "sweetalert2-react-content";
import { GlobalContext } from "../context/GlobalState";
import DataTable from 'react-data-table-component';


export const ProductList = () => {
  const { products, removeProduct, localSKUSearch, listProduct } = useContext(GlobalContext);
  const [_products, setLocalProducts] = useState();
  const router = useRouter();

  useEffect(() => {
    listProduct()
  }, [])

  useEffect(() => {
    setLocalProducts(products);
  }, [products]);

  const handleRemoveProductClick = (product) => {
    const MySwal = withReactContent(Swal);
    MySwal.fire({
      title: (
        <p>
          Do you want to delete product {product.sku} <br /> {product.product_name}?
        </p>
      ),
      showDenyButton: true,
      confirmButtonText: "OK",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        removeProduct(product.sku);
      }
    });
  };

  const handleSearchOnchange = (e) => {
    setLocalProducts(localSKUSearch(e.target.value));
  };


  const columns = [
    {
      name: 'SKU',
      selector: row => row.sku,
    },
    {
      name: 'Product Name',
      selector: row => row.product_name,
    },
    {
      name: "Action",
      selector: row => {
        return (
          <div className="flex justify-between text-right px-4 py-2 m-2">
            <button title="Edit Product" onClick={() => router.push(`/edit/${row.sku}`)} className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-semibold mr-3 py-2 px-4 rounded-full inline-flex items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-edit"
              >
                <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
                <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
              </svg>
            </button>
            <button
              onClick={() => handleRemoveProductClick(row)}
              className="block bg-gray-300 hover:bg-gray-400 text-gray-800 font-semibold py-2 px-4 rounded-full inline-flex items-center"
              title="Remove Product"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-trash-2"
              >
                <polyline points="3 6 5 6 21 6"></polyline>
                <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                <line x1="10" y1="11" x2="10" y2="17"></line>
                <line x1="14" y1="11" x2="14" y2="17"></line>
              </svg>
            </button>
          </div>
        )
      }
    }
  ];

  const data = _products

  return (
    <>
      <input
        className="shadow appearance-none border rounded w-full py-2 my-5 px-3 text-gray-700 leading-tight focus:text-gray-600 focus:shadow-outline"
        onChange={handleSearchOnchange}
        type="text"
        placeholder="Search product by SKU"
      />
      <DataTable
        columns={columns}
        data={data}
        pagination
      />
    </>
  );
};
