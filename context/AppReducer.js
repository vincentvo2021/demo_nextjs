export default function appReducer(state, action) {
  switch (action.type) {
    case "LIST_PRODUCT":
      return {
        ...state,
        products: action.payload,
      };

    case "ADD_PRODUCT":
      return {
        ...state,
        products: [...state.products, action.payload],
      };

    case "EDIT_PRODUCT":
      const updatedProduct = action.payload;

      const updatedProducts = state.products.map((product) => {
        if (product.sku === updatedProduct.sku) {
          return updatedProduct;
        }
        return product;
      });

      return {
        ...state,
        products: updatedProducts,
      };

    case "REMOVE_PRODUCT":
      return {
        ...state,
        products: state.products.filter(
          (product) => product.sku !== action.payload
        ),
      };

    case "LOGIN":
      return {
        ...state,
        token: action.payload,
      };

    default:
      return state;
  }
}
