import React, { useContext, useEffect, useState } from "react"

import { GlobalContext } from "../../context/GlobalState"
import Swal from "sweetalert2"
import { useRouter } from "next/router"
import withReactContent from "sweetalert2-react-content"

const Login = () => {
    const router = useRouter()
    const { authLogin } = useContext(GlobalContext)

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const onSubmit = async (e) => {
        e.preventDefault()
        const data = await authLogin(email, password)
        const MySwal = withReactContent(Swal)
        if (!data || !data.token) {
            MySwal.fire({
                title: <p>Login Fail</p>,
            }).then((result) => {
                router.push("/login")
            })
        } else {
            router.push("/")
        }
    }

    return (
        <>
            <div className="w-full max-w-sm container mt-20 mx-auto">
                <form onSubmit={onSubmit}>
                    <div className="w-full  mb-5">
                        <label
                            className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                            htmlFor="email"
                        >
                            Email
                        </label>
                        <input
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:text-gray-600 focus:shadow-outline"
                            value={email}
                            type="email"
                            placeholder="Email"
                            onChange={(e) => setEmail(e.target.value)}
                        />
                    </div>
                    <div className="w-full mb-5">
                        <label
                            className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                            htmlFor="password"
                        >
                            Password
                        </label>
                        <input
                            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:text-gray-600 focus:shadow-outline"
                            value={password}
                            type="password"
                            placeholder="Enter Password"
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <div className="flex items-center justify-center">
                        <button className="block mt-5 bg-green-400 w-full hover:bg-green-500 text-white font-bold py-2 px-4 rounded focus:text-gray-600 focus:shadow-outline">
                            Login
                        </button>
                    </div>
                    <div className="text-center mt-4 text-gray-500">
                        <button type="button" onClick={() => router.push("/register")}>
                            Register new account
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}
export default Login
