import React, { useContext, useState } from "react";

import { GlobalContext } from "../../context/GlobalState";
import { useRouter } from "next/router";

const AddProduct = () => {
  const router = useRouter();

  const { addProduct, products } = useContext(GlobalContext);

  const [product_name, setName] = useState("");
  const [sku, setSKU] = useState("");
  const [qty, setQty] = useState("");
  const [price, setPrice] = useState("");
  const [unit, setUnit] = useState("");
  const [status, setStatus] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();
    const newProduct = {
      sku,
      product_name,
      qty,
      price,
      unit,
      status,
    };
    addProduct(newProduct);
    router.push("/");
  };

  return (
    <>
      <div className="w-full max-w-sm container mt-20 mx-auto">
        <form onSubmit={onSubmit}>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              SKU
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:text-gray-600"
              value={sku}
              onChange={(e) => setSKU(e.target.value)}
              type="text"
              placeholder="Enter SKU"
            />
          </div>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Name
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:text-gray-600"
              value={product_name}
              onChange={(e) => setName(e.target.value)}
              type="text"
              placeholder="Enter name"
            />
          </div>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Qty
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:text-gray-600"
              value={qty}
              onChange={(e) => setQty(e.target.value)}
              type="number"
            />
          </div>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Price
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:text-gray-600"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
              type="text"
              placeholder="Enter Price"
            />
          </div>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Unit
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:text-gray-600"
              value={unit}
              onChange={(e) => setUnit(e.target.value)}
              type="text"
              placeholder="Enter Unit"
            />
          </div>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Status
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:text-gray-600"
              value={status}
              onChange={(e) => setStatus(e.target.value)}
              type="number"
              placeholder="Enter Status"
            />
          </div>
          <div className="flex items-center justify-between">
            <button className="mt-5 bg-green-400 w-full hover:bg-green-500 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
              Add Product
            </button>
          </div>
          <div className="text-center mt-4 text-gray-500">
            <button type="button" onClick={() => router.push("/")}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    </>
  );
};
export default AddProduct;
