import axios from "axios";

const headers = {
  "Content-Type": "application/json",
  "Access-Control-Allow-Headers": "*",
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Methods": "*",
};
const axiosClient = axios.create({
  baseURL: "http://localhost:5000/",
  headers: headers,
});

export default axiosClient;
