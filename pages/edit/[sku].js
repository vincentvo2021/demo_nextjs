import React, { useContext, useEffect, useState } from "react"

import { useRouter } from "next/router"
import { GlobalContext } from "../../context/GlobalState"

const EditProduct = () => {
  const router = useRouter()

  const { products, editProduct } = useContext(GlobalContext)

  const [selectedUser, setSelectedUser] = useState({
    id: null,
    sku: '',
    product_name: '',
    qty: '',
    price: '',
    unit: '',
    status: '',
  })

  const currentUserSKU = router.query.sku

  console.log(1111, router.query.sku, currentUserSKU)

  useEffect(() => {
    const selectedUser = products.find(
      (product) => {
        console.log(product)
        return product.sku === currentUserSKU
      }
    )
    setSelectedUser(selectedUser)
  }, [currentUserSKU, products])

  const onSubmit = (e) => {
    e.preventDefault()
    editProduct(selectedUser)
    router.push("/")
  }

  const handleOnChange = (userKey, newValue) =>
    setSelectedUser({ ...selectedUser, [userKey]: newValue })

  if (!selectedUser || !selectedUser.sku) {
    return <div>Invalid Product SKU.</div>
  }

  return (
    <>
      <div className="w-full max-w-sm container mt-20 mx-auto">
        <form onSubmit={onSubmit}>
          <div className="w-full  mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="sku"
            >
              SKU
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:text-gray-600 focus:shadow-outline"
              value={selectedUser.sku}
              onChange={(e) => handleOnChange("sku", e.target.value)}
              type="text"
              placeholder="SKU"
              disabled={true}
            />
          </div>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Name
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:text-gray-600 focus:shadow-outline"
              value={selectedUser.product_name}
              onChange={(e) => handleOnChange("product_name", e.target.value)}
              type="text"
              placeholder="Enter name"
            />
          </div>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Qty
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:text-gray-600 focus:shadow-outline"
              value={selectedUser.qty}
              onChange={(e) => handleOnChange("qty", e.target.value)}
              type="text"
              placeholder="Enter Qty"
            />
          </div>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Price
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:text-gray-600 focus:shadow-outline"
              value={selectedUser.price}
              onChange={(e) => handleOnChange("price", e.target.value)}
              type="text"
              placeholder="Enter Price"
            />
          </div>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Unit
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:text-gray-600 focus:shadow-outline"
              value={selectedUser.unit}
              onChange={(e) => handleOnChange("unit", e.target.value)}
              type="text"
              placeholder="Enter Unit"
            />
          </div>
          <div className="w-full mb-5">
            <label
              className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
              htmlFor="name"
            >
              Status
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:text-gray-600 focus:shadow-outline"
              value={selectedUser.status}
              onChange={(e) => handleOnChange("status", e.target.value)}
              type="text"
              placeholder="Enter Status"
            />
          </div>
          <div className="flex items-center justify-between">
            <button className="block mt-5 bg-green-400 w-full hover:bg-green-500 text-white font-bold py-2 px-4 rounded focus:text-gray-600 focus:shadow-outline">
              Save
            </button>
          </div>
          <div className="text-center mt-4 text-gray-500">
            <button onClick={() => router.push("/")}>Cancel</button>
          </div>
        </form>
      </div>
    </>
  )
}
export default EditProduct
