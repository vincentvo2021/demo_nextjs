import React, { createContext, useEffect, useReducer, useState } from "react"
import API from "../helpers/api"

import appReducer from "./AppReducer"

const initialState = {
  products: [],
  token: null,
}

export const GlobalContext = createContext(initialState)

export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(appReducer, initialState)

  async function listProduct() {
    const response = await API.get('/items', state.token)
    dispatch({
      type: "LIST_PRODUCT",
      payload: response,
    })
  }

  async function addProduct(product) {
    const response = await API.post('/item/add', product, state.token)
    dispatch({
      type: "ADD_PRODUCT",
      payload: response,
    })
  }

  function isLoggedIn() {
    return state.token != null
  }

  async function editProduct(product) {
    const response = await API.post('/item/update', product, state.token)
    dispatch({
      type: "EDIT_PRODUCT",
      payload: response,
    })
  }

  function localSKUSearch(sku) {
    return state.products.filter((item) =>
      item.sku.match(new RegExp(sku, "ig"))
    )
  }

  async function removeProduct(sku) {
    const response = await API.post('/item/delete', { sku }, state.token)
    dispatch({
      type: "REMOVE_PRODUCT",
      payload: response.sku,
    })
  }

  async function authRegister(email, password) {
    return API.post('/register', { email, password })
  }

  async function authLogin(email, password) {
    const response = await API.post('/auth/login', { email, password })
    dispatch({
      type: "LOGIN",
      payload: response.token,
    })

    return response
  }

  return (
    <GlobalContext.Provider
      value={{
        products: state.products,
        token: state.token,
        addProduct,
        editProduct,
        removeProduct,
        localSKUSearch,
        authLogin,
        authRegister,
        isLoggedIn,
        listProduct,
      }}
    >
      {children}
    </GlobalContext.Provider>
  )
}
